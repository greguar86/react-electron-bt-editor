import { Switch, BrowserRouter } from 'react-router-dom'
import React, { Component } from 'react';
import { Button } from 'semantic-ui-react'
import { Container, Divider, Grid, Header, Menu, Message, Segment, Table } from 'semantic-ui-react'

class ScreenDialogs extends Component {

  render() {
    return (

      <div >
        <br/>


      
 
        <Message
          attached
          content='Dialogs'
          icon='help circle'
          info
        />
        <Table attached='bottom'>
          <Table.Header>
            <Table.HeaderCell>Header</Table.HeaderCell>
            <Table.HeaderCell>Header</Table.HeaderCell>
            <Table.HeaderCell>Header</Table.HeaderCell>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>

      </div>


    );
  }
}

export default ScreenDialogs;
