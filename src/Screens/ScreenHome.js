import { Switch, BrowserRouter } from 'react-router-dom'
import React, { Component } from 'react';
import { Button } from 'semantic-ui-react'

const { ipcRenderer } = window.require('electron');
// w przegladarce sie wywala, ale electron dziala! 
// https://github.com/electron/electron/issues/9920

const emptyQuest = {
  questTitle: "empty quest",
  questID: -1,
  goldReward: 0,
  xpReward: 0,
  isSubquest: false,
}

const emptyDialog = {
  question: "empty question",
  answer: "empty answer",
}


class ScreenHome extends Component {

  state = { fromMain: '', quest: '', dialog: emptyDialog }
  
  closeWindow() {
    let fromMain = ipcRenderer.sendSync('close', 1)
    this.setState({ fromMain: fromMain, });
  }

  getQuest() {
    let fromMainRpc = ipcRenderer.sendSync('pythonSyncCall', 'get_quest', '5+15')
    this.setState({ quest: fromMainRpc });
  }

  getDialog() {
    let fromMainRpc = ipcRenderer.sendSync('pythonSyncCall', 'get_dialog', 'BCD')
    this.setState({ dialog: JSON.parse(fromMainRpc) });
  }

  clear() {
    this.setState({ quest: '', dialog: emptyDialog });
  }

  render() {
    return (
      
        <div >
         <br />
        from main ipc: {this.state.fromMain}
        <br />
        <br />
        dialog:
        <br />
        question ({this.state.dialog.question}) <br />
        answer ({this.state.dialog.answer})
        <br />
        <br />
        quest: {this.state.quest}  
        <br />

        <Button primary content='exit' onClick={() => this.closeWindow()} />

        <Button primary content='getQuest' onClick={() => this.getQuest()} />
        <Button primary content='getDialog' onClick={() => this.getDialog()} />
        <Button primary content='clear' onClick={() => this.clear()} />
 


        </div>
     

    );
  }
}

export default ScreenHome;
