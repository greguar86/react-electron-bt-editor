import { Switch, BrowserRouter } from 'react-router-dom'
import React, { Component } from 'react';
import { Button } from 'semantic-ui-react'
import { Container, Divider, Grid, Header, Menu, Message, Segment, Table } from 'semantic-ui-react'

class ScreenQuests extends Component {

  render() {
    return (

      <div >
        <br />

        <Message
          attached='top'
          content='Quests'
          icon='attention'
          warning
        />
        <Table attached>
          <Table.Header>
            <Table.HeaderCell>Header</Table.HeaderCell>
            <Table.HeaderCell>Header</Table.HeaderCell>
            <Table.HeaderCell>Header</Table.HeaderCell>
          </Table.Header>
          <Table.Body>
            <Table.Row>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
              <Table.Cell>Cell</Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
        <Menu attached='bottom' compact widths={3}>
          <Menu.Item as='a'>Item</Menu.Item>
          <Menu.Item as='a'>Item</Menu.Item>
          <Menu.Item as='a'>Item</Menu.Item>
        </Menu>



      </div>


    );
  }
}

export default ScreenQuests;
