import { Link, Switch, BrowserRouter, Route } from 'react-router-dom'
import React, { Component } from 'react';
import { Button } from 'semantic-ui-react'
import ScreenHome from '../Screens/ScreenHome'
import ScreenDialogs from '../Screens/ScreenDialogs'
import ScreenQuests from '../Screens/ScreenQuests'
import NavBar from './NavBar'

class Navigation extends Component {

  render() {
    return (
      <BrowserRouter>
        <div >
  
          <NavBar />

          <Switch>
            <Route exact path='/' component={ScreenHome} />
            <Route path='/dialogs' component={ScreenDialogs} />
            <Route path='/quests' component={ScreenQuests} />
          </Switch>

        </div>
      </BrowserRouter>

    );
  }
}

export default Navigation;
