from __future__ import print_function

import zerorpc

from dialogs import get_dialog
from quests import get_quest


# todo: sqlalchemy orm do sqlite !!!
# https://www.pythoncentral.io/introduction-to-sqlite-in-python/

class Api(object):
    @staticmethod
    def get_quest(arg):
        try:
            return get_quest(arg)
        except Exception as e:
            return "error: " + str(e) + " :("

    @staticmethod
    def get_dialog(arg):
        try:
            return get_dialog(arg)
        except Exception as e:
            return "error " + str(e)


def main():
    #get_quest('1')

    addr = 'tcp://127.0.0.1:4246'
    s = zerorpc.Server(Api())
    s.bind(addr)
    print('start running on')
    s.run()


if __name__ == '__main__':
    main()
