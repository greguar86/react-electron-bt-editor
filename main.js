// ./main.js
const { app, BrowserWindow, ipcMain } = require('electron')
const path = require('path')
const zerorpc = require("zerorpc");

let win = null;

function createWindow() {
  // Initialize the window to our specified dimensions
  win = new BrowserWindow({ width: 1000, height: 600, frame: false });


  // produkcja czy development ?
  let production = false;

  if (!production) { win.loadURL('http://localhost:3000'); } // REACT ! 
  else {
    win.loadURL(require('url').format({
      pathname: path.join(__dirname, './build/index.html'),  // LUB INDEX HTML z elektrona 
      protocol: 'file:',
      slashes: true
    }))
  }

  if (!production) win.webContents.openDevTools()


  win.on('closed', function () {
    win = null;
  });
}


app.on('ready', function () {
  createWindow();
});

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

app.on('window-all-closed', function () {
  if (process.platform != 'darwin') {
    app.quit();
  }
});


/*************************************************************
 * + python
 *************************************************************/

let pyProc = null

const createPyProc = () => {

  // jezeli port zajety to komunikat - ze apliakcja jest juz uruchomiona
  let port = '4246'
  let script = path.join(__dirname, 'py_backend', 'api.py')
  pyProc = require('child_process').spawn('python', [script, port])
  if (pyProc != null) {
    console.log('child process success')
  }
}

const exitPyProc = () => {
  pyProc.kill()
  pyProc = null
  pyPort = null
}

app.on('ready', createPyProc)
app.on('will-quit', exitPyProc)


/*************************************************************
 * communication with REACT (ipc)
 *************************************************************/

ipcMain.on('async', (event, arg) => {
  console.log('received in main: ' + arg)
  event.sender.send('async-reply', 'ANSWER FROM MAIN')
})

ipcMain.on('sync', (event, arg) => {
  console.log('received in main SYNC: ' + arg)
  event.returnValue = 4
})


ipcMain.on('close', (event, arg) => {
  event.returnValue = "FROM MAIN"
  win.close()
})

ipcMain.on('pythonSyncCall', (event, methodName, arg) => { 
  console.log("PYTHON SYNC")

  new Promise(callPythonMethod(methodName, arg)) 
    .then(function (result) { event.returnValue = result;  console.log(result) })
    .catch(function (error) { event.returnValue = "error" });

   


})

/*************************************************************
* communication with PYTHON (rpc)
*************************************************************/

let client = new zerorpc.Client()
client.connect("tcp://127.0.0.1:4246")

const callPythonMethod = (methodName, arg) => {
  return (resolve, reject) => {
    client.invoke(methodName, arg, (error, res) => {
      if (res) resolve(res)
      else reject(error);
    });
  }
}